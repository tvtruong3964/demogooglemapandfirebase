//
//  LoginViewController.swift
//  DemoGoogleMap
//
//  Created by Truong Tran on 7/7/17.
//  Copyright © 2017 Truong Tran. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
  
  // MARK: Constants
  let loginToList = "LoginToMap"
  
  // MARK: Outlets
  @IBOutlet weak var textFieldLoginEmail: UITextField!
  @IBOutlet weak var textFieldLoginPassword: UITextField!

  override func viewDidLoad() {
    super.viewDidLoad()
    FIRAuth.auth()!.addStateDidChangeListener() {
      auth, user in
      if user != nil {
        let currentUser = User(authData: user!)
        self.performSegue(withIdentifier: self.loginToList, sender: currentUser)
      }
    }
  }
  
  // MARK: Actions
  @IBAction func loginDidTouch(_ sender: AnyObject) {
    FIRAuth.auth()?.signIn(withEmail: textFieldLoginEmail.text!, password: textFieldLoginPassword.text!)          {
      firUser, error in
      if error == nil {
        print("***Login Success")
      } else {
        print("***Login error \(String(describing: error))")
      }
      
    }
    
  }
  
  @IBAction func signUpDidTouch(_ sender: AnyObject) {
    
    
    
    let alert = UIAlertController(title: "Register",
                                  message: "Register",
                                  preferredStyle: .alert)
    
    let saveAction = UIAlertAction(title: "Save",style: .default) {
      action in
      let emailField = alert.textFields![0]
      let passwordField = alert.textFields![1]
      
      FIRAuth.auth()!.createUser(withEmail: emailField.text!, password: passwordField.text!)
      {
        firUser, error in
        if error == nil {
          print("***sigin Success")
          FIRAuth.auth()!.signIn(withEmail: self.textFieldLoginEmail.text!, password: self.textFieldLoginPassword.text!)
                    {
                      firUser, error in
                        if error == nil {
                          print("***Login Success")
                        } else {
                          print("***Login error \(String(describing: error))")
                        }
          
                    }
        } else {
          print("***sigin error \(String(describing: error))")
        }
      }
    }
    
    let cancelAction = UIAlertAction(title: "Cancel",
                                     style: .default)
    
    alert.addTextField { textEmail in
      textEmail.placeholder = "Enter your email"
    }
    
    alert.addTextField { textPassword in
      textPassword.isSecureTextEntry = true
      textPassword.placeholder = "Enter your password"
    }
    
    alert.addAction(saveAction)
    alert.addAction(cancelAction)
    
    present(alert, animated: true, completion: nil)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let navigationController = segue.destination as! UINavigationController
    let controller = navigationController.viewControllers.first as! MapViewController
    controller.currentUser = sender as! User
  }
  
}

extension LoginViewController: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField == textFieldLoginEmail {
      textFieldLoginPassword.becomeFirstResponder()
    }
    if textField == textFieldLoginPassword {
      textField.resignFirstResponder()
    }
    return true
  }
  
}
