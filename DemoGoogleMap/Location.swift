//
//  Location.swift
//  DemoGoogleMap
//
//  Created by Truong Tran on 7/6/17.
//  Copyright © 2017 Truong Tran. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase

class Location: NSObject {
  var name: String?
  var latitude: Double?
  var longitude: Double?
 // var marker: GMSMarker?
  
		
  let ref: FIRDatabaseReference?
  
  init(name: String, latitude: Double, longitude: Double) {
    self.name = name
    self.latitude = latitude
    self.longitude = longitude
    
    self.ref = nil
    
//    marker = GMSMarker(position: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
//    marker!.title = name


  }
  init?(snapshot: FIRDataSnapshot) {
    let snapshootValue = snapshot.value as! [String: Any]
    guard let name = snapshootValue["name"] as? String, let latitude = snapshootValue["latitude"] as? Double, let longitude = snapshootValue["longitude"] as? Double  else {
      return nil
    }
    
    self.name = name
    self.latitude = latitude
    self.longitude = longitude
    self.ref = snapshot.ref
    
    
//    CATransaction.begin()
//    CATransaction.setAnimationDuration(1.0)
//    marker = GMSMarker(position: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
//    marker!.title = name
//    CATransaction.commit()

  }
  
  
  func toAnyObject() -> Any {
    return [
      "name": name!,
      "latitude": latitude!,
      "longitude": longitude!
    ]
  }
}
