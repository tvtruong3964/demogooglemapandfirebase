//
//  ViewController.swift
//  DemoGoogleMap
//
//  Created by Truong Tran on 7/6/17.
//  Copyright © 2017 Truong Tran. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase

class MapViewController: UIViewController {
  @IBOutlet weak var mapView: GMSMapView!
  @IBOutlet weak var addressLabel: UILabel!
  
  var marker: GMSMarker?
  var currentUser: User!
  let locationManager = CLLocationManager()
  
  lazy var usersRef = FIRDatabase.database().reference(withPath: "demo-googlemap").child("realtime-location")
  
  lazy var usersSaveLocationRef = FIRDatabase.database().reference(withPath: "demo-googlemap").child("save-location")
  //var refUserLocation = self.ref.child("user-location")
  var currentUserRef: FIRDatabaseReference?
  var currentUserRefHandle: FIRDatabaseHandle?
		
  var currentLocation: CLLocationCoordinate2D?
  
  var locations: [Location] = []
  // create time update location
  var timer: Timer!
		
  
  override func viewDidLoad() {
    super.viewDidLoad()
    locationManager.delegate = self
    locationManager.requestWhenInUseAuthorization()
    
    locationManager.startUpdatingLocation()
    mapView.isMyLocationEnabled = true
    mapView.settings.myLocationButton = true
    
    
    print("Got user id: \(currentUser.uid)")
    currentUserRef = usersRef.child(currentUser.uid) // set key dictionary
    
    currentUserRefHandle = usersRef.observe(.value, with: {
      snapshot in
        var newItems = [Location]()
        for item in snapshot.children {
          if let locationItem = Location(snapshot: item as! FIRDataSnapshot) {
            newItems.append(locationItem)
            print("***\(locationItem.latitude!) : \(locationItem.longitude!)")
//            print("ref: \(locationItem.ref!.key)")
          }
        }
        self.mapView.clear() // clear all marker before update it
        self.locations = newItems
        self.updateMaker(locations: self.locations)
      
    })
    
    
    
    // set default location
    let defaultLocation =  Location(name: "Default", latitude: 10.772723, longitude: 106.698168)
      
    
    let defauleLocationCoordinate2D = CLLocationCoordinate2D(latitude: defaultLocation.latitude!, longitude: defaultLocation.longitude!)
    mapView.camera = GMSCameraPosition(target: defauleLocationCoordinate2D, zoom: 15, bearing: 0, viewingAngle: 0)
    
    let maker = GMSMarker(position: defauleLocationCoordinate2D)
    maker.title = defaultLocation.name!
    maker.map = mapView
    
    let weakReferenceMapViewController = WeakReferenceMapViewController(mapViewController: self)
    
    // create timer
    timer = Timer.scheduledTimer(timeInterval: 1, target: weakReferenceMapViewController, selector: #selector(weakReferenceMapViewController.timerFunctionSaveRealTimeLocation), userInfo: nil, repeats: true)
    
  }
  
  private func updateMaker(locations: [Location]) {
    for location in locations {
      
      // not create and update maker current user
      if location.ref!.key.isEqual(currentUser.uid) {
        continue
      }
      
      CATransaction.begin()
      CATransaction.setAnimationDuration(1.0)
      
      let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: location.latitude!, longitude: location.longitude!))
      marker.title = location.name

      marker.map = mapView
      CATransaction.commit()
      
    }
  }
  
//  private func setMapCamera(loaction: Location) {
//    
//    let locationCoordinate2D = CLLocationCoordinate2D(latitude: loaction.latitude!, longitude: loaction.longitude!)
//    
//    //    CATransaction.begin()
//    //    CATransaction.setValue(2, forKey: kCATransactionAnimationDuration)
//    //
//    //    mapView.animate(to: GMSCameraPosition(target: locationCoordinate2D, zoom: 15, bearing: 0, viewingAngle: 0))
//    //    CATransaction.commit()
//    
//    // marker?.map = nil
//    //  marker = nil
//    if marker == nil {
//      marker = GMSMarker(position: locationCoordinate2D)
//      marker!.title = loaction.name!
//      marker!.map = mapView
//    } else {
//      CATransaction.begin()
//      CATransaction.setAnimationDuration(1.0)
//      marker?.position = locationCoordinate2D
//      CATransaction.commit()
//    }
//    
//    
//  }
  
//  @IBAction func onNext(_ sender: UIBarButtonItem) {
//    setMapCamera(loaction: Location(name: currentUser.email, latitude: 10.778731, longitude: 106.698780))
//  }
//  
//  @IBAction func onChange(_ sender: UIBarButtonItem) {
//    setMapCamera(loaction: Location(name: currentUser.email, latitude: 10.779764, longitude: 106.698984))
//    
//  }
  
  @IBAction func onLogout(_ sender: UIBarButtonItem) {
    // remove value if logout
    
    for (index, location) in locations.enumerated() {
      if location.ref!.key.isEqual(currentUser.uid) {
        locations.remove(at: index)
        location.ref!.removeValue()
      }
    }
    try! FIRAuth.auth()!.signOut()
    dismiss(animated: true, completion: nil)
  }
//  @IBAction func onShowLocation(_ sender: UIBarButtonItem) {
//    saveLocationToFire()
//  }
  
  func saveRealTimeLocationToFire() {
//    print("Latitude: \(String(describing: locationManager.location?.coordinate.latitude)) Longitude: \(String(describing: locationManager.location?.coordinate.longitude))")
    
    print("*** Go to save location")
    guard let newLocation = locationManager.location?.coordinate else {
      return
    }
    
    // only update to fire different previous location
    guard !(currentLocation?.latitude == newLocation.latitude && newLocation.longitude == locationManager.location?.coordinate.longitude)  else {
      return
    }
    let location = Location(name: currentUser.email, latitude: newLocation.latitude, longitude: newLocation.longitude)
    
    //    let UserLocationRef = currentUserRef?.childByAutoId()
    //    UserLocationRef?.setValue(location.toAnyObject())
    currentUserRef?.setValue(location.toAnyObject())
    currentUserRef?.onDisconnectRemoveValue() // remove value if disconnect(exit app)
    
    currentLocation = newLocation
    
  }
  
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  deinit {
    
    timer.invalidate()
    if let refHandle = currentUserRefHandle {
      usersRef.removeObserver(withHandle: refHandle)
    }
    
    print("deinit")
  }
  
  private class WeakReferenceMapViewController {
    weak var mapViewController: MapViewController?
    init(mapViewController: MapViewController) {
      self.mapViewController = mapViewController
    }
    
    @objc func timerFunctionSaveRealTimeLocation(timer: Timer?) {
      mapViewController?.saveRealTimeLocationToFire()
    }
  }
  
  
}

extension MapViewController: CLLocationManagerDelegate {
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)  {
    switch status {
    case .restricted:
      print("Location access was restricted.")
    case .denied:
      print("User denied access to location.")
    case .notDetermined:
      print("Location status not determined.")
    case .authorizedAlways: fallthrough
    case .authorizedWhenInUse:
      print("Location status is OK.")
    }
  }
  
  
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    if let location = locations.last {
      print("Location: \(location)")
      mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
      //  currentLocation = location.coordinate
      
      locationManager.stopUpdatingLocation()
      
      saveRealTimeLocationToFire()
    }
    
  }
  

}

