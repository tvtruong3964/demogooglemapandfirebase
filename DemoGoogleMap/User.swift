//
//  User.swift
//  DemoGoogleMap
//
//  Created by Truong Tran on 7/7/17.
//  Copyright © 2017 Truong Tran. All rights reserved.
//

import Foundation
import Firebase
struct User {
  
  let uid: String
  let email: String
  
  init(authData: FIRUser) {
    uid = authData.uid
    email = authData.email!
  }
  
  init(uid: String, email: String) {
    self.uid = uid
    self.email = email
  }
  
}
